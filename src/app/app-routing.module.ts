import { NotificationsComponent } from './pages/notifications/notifications.component';
import { SearchResultComponent } from './pages/search-result/search-result.component';
import { NgModule } from '@angular/core';
import { SearchChildrenComponent } from './pages/search-children/search-children.component';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CampusAnonymousComponent } from './pages/friends-info/campus-anonymous.component';
import { ConfirmAndShareComponent } from './pages/confirm-and-share/confirm-and-share.component';
import { HowItWorksComponent } from './pages/how-it-works/how-it-works.component';
import { LandingPageComponent } from './pages/welcome/landing-page.component';
import { LoginComponent } from './pages/login/login.component';
import { NotReadyToShareComponent } from './pages/not-ready-to-share/not-ready-to-share.component';
import { RegisterComponent } from './pages/register/register.component';
import { SubmissionConfirmatoinComponent } from './pages/submission-confirmatoin/submission-confirmatoin.component';
import { VerifyOtpComponent } from './pages/verify-otp/verify-otp.component';
import { CreatePasswordComponent } from './pages/create-password/create-password.component';
import { ReviewInfoComponent } from './pages/review-info/review-info.component';
import { RewardOptionComponent } from './pages/reward-option/reward-option.component';
import { ConfirmToShareComponent } from './pages/confirm-to-share/confirm-to-share.component';
import { ForgetPasswordComponent } from './pages/forget-password/forget-password.component';
import { UpdatePasswordComponent } from './pages/update-password/update-password.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { TermsComponent } from './pages/terms/terms.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landing',
    pathMatch: 'full'
  },
  {
      path: 'landing',
      component: LandingPageComponent 
  },
  {
      path: 'login',
      component: LoginComponent  
  },
  {
    path: 'campus-aonoymous',
    component: CampusAnonymousComponent
  },
  {
    path: 'how-it-works',
    component: HowItWorksComponent
  },
  {
    path: 'not-ready-to-share',
    component: NotReadyToShareComponent
  },
  {
    path: 'confirm-and-share',
    component: ConfirmAndShareComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'forget-password',
    component: ForgetPasswordComponent
  },
  {
    path: 'verification',
    component: VerifyOtpComponent
  },
  {
    path: 'update-password',
    component: UpdatePasswordComponent
  },
  {
    path: 'create-password',
    component: CreatePasswordComponent
  },
  {
    path: 'review-info',
    component: ReviewInfoComponent
  },
  {
    path: 'reward-option',
    component: RewardOptionComponent
  },
  {
    path: 'confirm-to-share',
    component: ConfirmToShareComponent
  },
  {
    path: 'submission-confirmation',
    component: SubmissionConfirmatoinComponent
  },
  {
    path: 'search-children',
    component: SearchChildrenComponent
  },
  {
    path: 'search-result',
    component: SearchResultComponent
  },
  {
    path: 'notifications',
    component: NotificationsComponent
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent
  },
  {
    path: 'terms',
    component: TermsComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
