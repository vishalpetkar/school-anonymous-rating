import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Profile } from 'src/app/models/school-model';
import { SchoolServiceService } from 'src/app/services/school-service.service';
import { ProfileState } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss'],
})
export class UpdatePasswordComponent implements OnInit {

  public ChangeForm: FormGroup;
  public tempUserData = new Profile();
  public userData = new Profile();
  @Select(ProfileState.tempUserKey) tempprofile$: Observable<Profile>;
  @Select(ProfileState.profileKey) profile$: Observable<Profile>;
  constructor(private router: Router,
              public formBuilder: FormBuilder,
              public loadingController: LoadingController,
              private authService: SchoolServiceService) {

    this.ChangeForm = formBuilder.group({
      mobile: ['', [Validators.required]],
      new_password: ['', Validators.required],
      new_confirm_password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.profile$.subscribe(data => {
      this.userData = data;
      console.log("profileData", this.userData);      
    });

    this.tempprofile$.subscribe(data => {
      this.tempUserData = data;
      console.log("Temp User Data", this.tempUserData);
    });
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }

  change() {
    console.log(this.ChangeForm.value);
    this.ChangeForm.get('mobile').setValue(this.tempUserData.mobile);
    this.authService.updatePassword(this.ChangeForm.value).subscribe(success => {
        console.log(success);
        this.ChangeForm.reset();
        this.router.navigate(['/login']);
      }, err => {
    });
  }

}
