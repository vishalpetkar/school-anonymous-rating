import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { Profile } from 'src/app/models/school-model';
import { GetProfile } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [FCM]
})
export class LoginComponent implements OnInit {

  android_token: string = '';
  public tempUserData = new Profile();
  email: string = '';
  password: string = '';

  constructor(
    private platform: Platform,
    private fcm: FCM,
    private router: Router,
    private store: Store) { }

  ngOnInit() {
    this.platform.ready().then(() => {
      try {

          // Get FCM token
          this.fcm.getToken().then(token => {
            this.android_token = token;
          }).catch(err=>{
          });

          // ionic push notification example
          this.fcm.onNotification().subscribe(data => {
            console.log('Notification Recieved..');
          },err=>{
          });

          // Refresh the FCM token
          this.fcm.onTokenRefresh().subscribe(token => {
            this.android_token = token;
          },err=>{
          });
      } catch (error) {
      }
    });
  }

  login() {
    this.tempUserData.email = this.email;
    this.tempUserData.password = this.password;
    this.store.dispatch(new GetProfile({data: this.tempUserData})).subscribe(success => {
      this.email = '';
      this.password = '';
      this.router.navigate(['/campus-aonoymous']);
    });
  }
}
