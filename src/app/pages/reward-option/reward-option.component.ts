import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { Brand } from 'src/app/models/school-model';
import { ProfileState, KeepFriendsInfo, GetBrand, SelectedBrand } from 'src/app/store/profile.actions';
import { RewardPopupComponent } from '../reward-popup/reward-popup.component';

@Component({
  selector: 'app-reward-option',
  templateUrl: './reward-option.component.html',
  styleUrls: ['./reward-option.component.scss'],
})
export class RewardOptionComponent implements OnInit {

  reward: string = '';
  @Select(ProfileState.tempFriendInfoKey) tempFriendsInfo$: Observable<FriendsInfo>;
  @Select(ProfileState.brandKey) brand$: Observable<Array<Brand>>;
  tempFriendInfo: FriendsInfo = new FriendsInfo();
  brandData: Array<Brand> = [];

  constructor(
    public alertController: AlertController,
    private router: Router,
    private store: Store,
    private modalController: ModalController,
    public toastController: ToastController) { }

  ngOnInit() {
    this.tempFriendsInfo$.subscribe(friendsInfo => {
      this.tempFriendInfo = friendsInfo;
    });

    this.store.dispatch(new GetBrand()).subscribe(data => {
    });

    this.brand$.subscribe(data => {
      this.brandData = data;
    });
  }

  async selectedBrand(brand) {
    this.reward = brand.brand_code;
    this.store.dispatch(new SelectedBrand({data: brand})).subscribe(data => {
    });
    this.tempFriendInfo.reward = brand.brand_code;
    // this.rewardPopup();

    const toast = await this.toastController.create({
      message: 'You Selected ' + brand.brand_code + '.',
      duration: 2000
    });
    toast.present();
  }

  async rewardPopup() {
    const rewardPopup = await this.modalController.create({
      component: RewardPopupComponent,
      cssClass: "search-popup",
      componentProps: {},
    });
    rewardPopup.onDidDismiss().then((data) => {
      console.log(data, data.data.price);
      this.tempFriendInfo.reward_price = data.data.price;  
    });
    return await rewardPopup.present().then(() => {
      console.log("popup loaded");
    });
  }

  async confirmInfo() {
    console.log("confirm info..", this.tempFriendInfo);

    if (this.tempFriendInfo.reward == "") {

      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Alert',
        subHeader: '',
        message: 'Please select a brand',
        buttons: [
          {
            text: 'Okay',
            role: 'ok',
            handler: () => {
              console.log('Okay');
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.store.dispatch(new KeepFriendsInfo({data: this.tempFriendInfo})).subscribe(success => {
        this.router.navigate(['/confirm-to-share']);
      });
    }

  }
}
