import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Profile } from 'src/app/models/school-model';
import { SendOTP, TempProfile } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss'],
})
export class ForgetPasswordComponent implements OnInit {

  phone: string = '';
  tempProfile: Profile = new Profile();

  constructor(
    private router: Router,
    private store: Store) { }

  ngOnInit() {}

  sendOtp() {
    this.tempProfile.mobile = this.phone;
    this.tempProfile.action = 'forget';
    console.log('temp profile', this.tempProfile);

    this.store.dispatch(new SendOTP({data: this.tempProfile})).subscribe(success => {
      this.store.dispatch(new TempProfile({data: this.tempProfile})).subscribe(success => {
        this.router.navigate(['/verification']);
      });
    });
  }

}
