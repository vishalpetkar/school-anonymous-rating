import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Notifications } from 'src/app/models/school-model';
import { GetNotification, PayGiftCard, ProfileState } from 'src/app/store/profile.actions';
import { ReviewComponent } from '../review/review.component';
import { RewardPopupComponent } from '../reward-popup/reward-popup.component';
import { AddRating } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {

  @Select(ProfileState.notificationKey) notif$: Observable<Array<Notifications>>;
  notificationsData: Array<Notifications> = [];
  @Select(ProfileState.giftcardRespKey) giftcardresp$: Observable<string>;
  gift_card_resp: string = "";

  constructor(
    private modalController: ModalController,
    public store: Store,
    public alertController: AlertController) { }

  ngOnInit() {

    this.store.dispatch(new GetNotification()).subscribe(result => {});

    this.notif$.subscribe(data => {
      this.notificationsData = data;
      console.log('this.notificationsData', this.notificationsData);
    });

    this.giftcardresp$.subscribe(async data => {
      console.log('gift card resp = ', data);
      if (data && !!data) {
        const arr = JSON.parse(data);
        let code = '', name = '', message = '', status = '';

        if (arr["info"] && !!arr["info"]) {
          status = 'Success!!';
          code = arr["info"]["code"];
          name = arr["info"]["name"];
          message = arr["info"]["message"];  
        }
        if (arr["error"] && !!arr["error"]) {
          status = 'Error!!';
          code = arr["error"]["code"];
          name = arr["error"]["name"];
          message = arr["error"]["message"];  
        }

        console.log(arr, code, name, message);
        
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: status,
          subHeader: name,
          message: message,
          buttons: [
            {
              text: 'Okay',
              role: 'ok',
              handler: () => {
                console.log('Okay..');
                this.store.dispatch(new GetNotification()).subscribe(result => {});
              }
            }
          ]
        });
        await alert.present();
      }
    });
  }

  async payGiftCard(item: Notifications) {
    console.log('payGiftCard id = ' + item.id);

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm',
      subHeader: '',
      message: 'To view message, you need to pay a new gift card <b>('+item.reward+')</b>.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          role: 'ok',
          handler: () => {
            console.log('Confirm Okay');
            this.rewardPopup(item);
          }
        }
      ]
    });
    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  // getLink(notification: Notifications) {
  //   console.log('notification = ', notification);
  //   var data = JSON.parse(notification["gift_card_resp"]);
  //   if (data["info"]["code"] == "INFO_CAMPAIGN_CREATED") {
  //     console.log('gift link = ', data, data["gift_link"]);
  //     window.open(data["gift_link"]);
  //   }
  // }

  async showMessage(txt) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Message',
      subHeader: '',
      message: txt,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async openReview(id) {
    const childrenDetailPopup = await this.modalController.create({
      component: ReviewComponent,
      cssClass: "search-popup",
      componentProps: {
        id
      },
    });
    childrenDetailPopup.onDidDismiss().then((data) => {
      this.store.dispatch(new GetNotification()).subscribe(result => {});
    });
    return await childrenDetailPopup.present().then(() => {
      console.log("popup loaded");
    });
  }

  async rewardPopup(item) {
    const rewardPopup = await this.modalController.create({
      component: RewardPopupComponent,
      cssClass: "search-popup",
      componentProps: {},
    });
    rewardPopup.onDidDismiss().then((data) => {
      console.log(data, data.data.price);
      this.store.dispatch(new PayGiftCard({data: {gift_card_id: item.id, reward_price:data.data.price} })).subscribe((data) => {});
    });
    return await rewardPopup.present().then(() => {
      console.log("popup loaded");
    });
  }
}
