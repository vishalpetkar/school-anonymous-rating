import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { ProfileState } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-not-ready-to-share',
  templateUrl: './not-ready-to-share.component.html',
  styleUrls: ['./not-ready-to-share.component.scss'],
})
export class NotReadyToShareComponent implements OnInit {

  @Select(ProfileState.tempFriendInfoKey) tempFriendsInfo$: Observable<FriendsInfo>; 
  tempFriendInfo: FriendsInfo = new FriendsInfo();

  constructor() { }

  ngOnInit() {
    this.tempFriendsInfo$.subscribe(friendsInfo => {
      console.log('friendsInfo ', friendsInfo);
      this.tempFriendInfo = friendsInfo;
      console.log('temp friend info ', this.tempFriendInfo);
      
    });
  }

}
