import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { RateChildren } from 'src/app/models/friend-model';
import { AddRating } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit {

  id: number;
  message: string = '';
  rating: number = 0;

  constructor(
    private modalController: ModalController,
    private store: Store) { }

  ngOnInit() {}

  change_callback(ele) {
    console.log(ele);
  }

  submit() {

    console.log(this.message, this.rating);

    const data = new RateChildren();
    data.id = this.id;
    data.message = this.message;
    data.rating = this.rating;
    this.store.dispatch(new AddRating({data: data})).subscribe(data => {
      this.modalController.dismiss();
    });
  }
}
