import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { ProfileState } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-children-detail',
  templateUrl: './children-detail.component.html',
  styleUrls: ['./children-detail.component.scss'],
})
export class ChildrenDetailComponent implements OnInit {


  child_id = 0;
  @Select(ProfileState.friendsInfoKey) friendsInfo$: Observable<Array<FriendsInfo>>; 
  friendInfo: Array<FriendsInfo>= [];
  friendInfoDetail: FriendsInfo = new FriendsInfo();

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    console.log("child_id = " + this.child_id);

    this.friendsInfo$.subscribe(friendsInfo => {
      this.friendInfo = friendsInfo;
      console.log("data", this.friendInfo);

      const idx = this.friendInfo.findIndex(obj => obj.id === this.child_id);
      console.log('record', this.friendInfo[idx]);
      this.friendInfoDetail = this.friendInfo[idx];
    });
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
