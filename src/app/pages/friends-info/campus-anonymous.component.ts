import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { FriendsInfo } from 'src/app/models/friend-model';
import { StorageService } from 'src/app/services/storage.service';
import { AddFriendsInfo, KeepFriendsInfo } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-campus-anonymous',
  templateUrl: './campus-anonymous.component.html',
  styleUrls: ['./campus-anonymous.component.scss'],
  providers: [ Storage ]
})
export class CampusAnonymousComponent implements OnInit {

  city: string = '';
  state: string = '';
  country: string = '';
  name: string = '';
  email: string = '';
  phone: string = '';
  school: string = '';
  grade: string = '';

  tempFriendInfo: FriendsInfo = new FriendsInfo();

  constructor(
    private alertController: AlertController,
    private router: Router,
    private store: Store,
    private storage: StorageService) { }

  ngOnInit() {
    
  }

  ionViewDidEnter() {
    this.storage.getObject('friends_info').then((friendsInfo) => {
      console.log('friendsInfo', friendsInfo);
      if (friendsInfo == null) {
        console.log('log 1 ', friendsInfo);
        this.tempFriendInfo = new FriendsInfo();
        this.name = '';
        this.email = '';
        this.phone = '';
        this.school = '';
        this.grade = '';
        this.city = '';
        this.state = '';
        this.country = '';
      } else {
        console.log('log 2 ', friendsInfo);
        this.tempFriendInfo = friendsInfo;
      }

    });
  }

  getFromDraft() {
    this.name = this.tempFriendInfo.name;
    this.email = this.tempFriendInfo.email;
    this.phone = this.tempFriendInfo.phone;
    this.school = this.tempFriendInfo.school;
    this.grade = this.tempFriendInfo.grade;
    this.city = this.tempFriendInfo.city;
    this.state = this.tempFriendInfo.state;
    this.country = this.tempFriendInfo.country;
  }

  share(type = 'ready') {

    if (this.name == '') {
      this.errorModal("Please enter your friend name.", 422);
      return false;
    }
    if (this.phone == '') {
      this.errorModal("Please enter your friend phone number.", 422);
      return false;
    }
    if (this.email == '') {
      this.errorModal("Please enter your friend email.", 422);
      return false;
    }
    if (this.school == '') {
      this.errorModal("Please enter school name.", 422);
      return false;
    }
    if (this.grade == '') {
      this.errorModal("Please enter grade.", 422);
      return false;
    }

    console.log("ready to share info");
    const friendsInfo = new FriendsInfo();
    friendsInfo.city = this.city;
    friendsInfo.state = this.state;
    friendsInfo.country = this.country;
    friendsInfo.name = this.name;
    friendsInfo.email = this.email;
    friendsInfo.phone = this.phone;
    friendsInfo.school = this.school;
    friendsInfo.grade = this.grade;
    this.store.dispatch(new KeepFriendsInfo({data: friendsInfo})).subscribe(success => {

      this.storage.setObject((friendsInfo), 'friends_info');
      this.name = '';
      this.email = '';
      this.phone = '';
      this.school = '';
      this.grade = '';
      this.city = '';
      this.state = '';
      this.country = '';

      if (type == 'ready') {
        this.router.navigate(['/review-info']);
      } else {
        this.router.navigate(['/not-ready-to-share']);
      }
    });
  }

  // Error Modal
  async errorModal(msg, code) {
      const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Error',
          subHeader: '',
          message: msg,
          buttons: ['OK']
      });
      await alert.present();
  }

}
