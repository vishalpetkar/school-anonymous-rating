import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { ProfileState, KeepFriendsInfo, AddFriendsInfo, GetFriendsInfo } from 'src/app/store/profile.actions';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

@Component({
  selector: 'app-confirm-to-share',
  templateUrl: './confirm-to-share.component.html',
  styleUrls: ['./confirm-to-share.component.scss'],
  providers: [Geolocation, LocationAccuracy, AndroidPermissions]
})
export class ConfirmToShareComponent implements OnInit {

  @Select(ProfileState.tempFriendInfoKey) tempFriendsInfo$: Observable<FriendsInfo>; 
  tempFriendInfo: FriendsInfo = new FriendsInfo();

  constructor(
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private router: Router,
    private store: Store) { }

  ngOnInit() {
    this.tempFriendsInfo$.subscribe(friendsInfo => {
      this.tempFriendInfo = friendsInfo;
    });

    this.checkGPSPermission();
  }

  submit() {
    console.log("submission confirmation");

    this.store.dispatch(new GetFriendsInfo({data: this.tempFriendInfo})).subscribe(success => {
      console.log(success);
      this.router.navigate(['/submission-confirmation']);
    });
    // this.store.dispatch(new AddFriendsInfo({data: this.tempFriendInfo})).subscribe(success => {
    //   this.router.navigate(['/submission-confirmation']);
    // });
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              // alert('requestPermission Error requesting location permissions ' + error)
              
            }
          );
      }
    });
  }

  //Check if application having GPS access permission  
  checkGPSPermission() {
    // return ;

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
 
          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {
 
          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationAndLoadData();
      },
      error => { 
        // alert('Error requesting location permissions ' + JSON.stringify(error))
        // alert('Please allow access the location service, to get shops near by you.');
        this.checkGPSPermission();
      }
    );
  }

  getLocationAndLoadData() {
    this.geolocation.getCurrentPosition({ maximumAge: 1000, timeout: 5000, enableHighAccuracy: true }).then(async (resp) => {
      console.log('location', resp, resp.coords.longitude, resp.coords.latitude);

      this.tempFriendInfo.lat = resp.coords.latitude;
      this.tempFriendInfo.lng = resp.coords.longitude;
    });
  }

}
