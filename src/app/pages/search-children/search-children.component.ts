import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { Notifications } from 'src/app/models/school-model';
import { StorageService } from 'src/app/services/storage.service';
import { AddFriendsInfo, GetFriendsInfo, GetNotification, ProfileState } from 'src/app/store/profile.actions';
import { ChildrenDetailComponent } from '../children-detail/children-detail.component';

@Component({
  selector: 'app-search-children',
  templateUrl: './search-children.component.html',
  styleUrls: ['./search-children.component.scss'],
})
export class SearchChildrenComponent implements OnInit {

  // name: string = 'paul';
  // email: string = 'paul@gmail.com';
  // phone: string = '8484976545';
  // city: string = 'nagpur';
  // state: string = 'mh';
  // country: string = 'india';
  // school: string = '';
  // grade: string = '';

  name: string = '';
  email: string = '';
  phone: string = '';
  city: string = '';
  state: string = '';
  country: string = '';
  school: string = '';
  grade: string = '';

  @Select(ProfileState.notificationKey) notif$: Observable<Array<Notifications>>;
  notificationsData: Array<Notifications> = [];

  @Select(ProfileState.friendsInfoKey) friendsInfo$: Observable<Array<FriendsInfo>>; 
  friendInfo: Array<FriendsInfo> = [];

  constructor(
    private storage: StorageService,
    private modalController: ModalController,
    private router: Router,
    private store: Store) { }

  ngOnInit() {

    console.log('get notifications..');
    this.store.dispatch(new GetNotification()).subscribe(result => {});

    this.notif$.subscribe(data => {
      this.notificationsData = data;
      console.log('this.notificationsData', this.notificationsData);
    });

    this.friendsInfo$.subscribe(friendsInfo => {
      this.friendInfo = friendsInfo;
      console.log("data", this.friendInfo);
    });

    // this.store.dispatch(new GetFriendsInfo({data: this.searchTxt})).subscribe(data => {
    //   this.friendInfo = data;
    // });
  }

  change_callback(ele) {
    console.log('called..', ele);
  }

  searchChildren() {
    // this.store.dispatch(new GetFriendsInfo({data: this.searchTxt})).subscribe(data => {
    //   this.friendInfo = data;
    // });
  }

  async detailPopup(child_id: number) {
    console.log('child_id', child_id);
    
    const childrenDetailPopup = await this.modalController.create({
      component: ChildrenDetailComponent,
      cssClass: "search-popup",
      componentProps: {
        child_id:child_id
      },
    });
    childrenDetailPopup.onDidDismiss().then((data) => {});
    return await childrenDetailPopup.present().then(() => {
      console.log("popup loaded");
    });
  }

  submit() {
    const friendsInfo = new FriendsInfo();
    friendsInfo.city = this.city;
    friendsInfo.state = this.state;
    friendsInfo.country = this.country;
    friendsInfo.name = this.name;
    friendsInfo.email = this.email;
    friendsInfo.phone = this.phone;
    friendsInfo.school = this.school;
    friendsInfo.grade = this.grade;
    // this.store.dispatch(new GetFriendsInfo({data: friendsInfo})).subscribe(success => {
    //     this.router.navigate(['/search-result']);
    // });
    this.store.dispatch(new AddFriendsInfo({data: friendsInfo})).subscribe(success => {
      // this.router.navigate(['/search-result']);
      this.name = '';
      this.email = '';
      this.phone = '';
      this.city = '';
      this.state = '';
      this.country = '';
      this.school = '';
      this.grade = '';
    });
  }
}

