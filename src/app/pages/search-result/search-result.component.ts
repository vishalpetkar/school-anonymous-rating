import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { Notifications } from 'src/app/models/school-model';
import { StorageService } from 'src/app/services/storage.service';
import { GetNotification, ProfileState } from 'src/app/store/profile.actions';
import { ReviewComponent } from '../review/review.component';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
})
export class SearchResultComponent implements OnInit {

  @Select(ProfileState.friendsInfoKey) friendsInfo$: Observable<Array<FriendsInfo>>; 
  friendInfo: Array<FriendsInfo> = [];
  searchResultFound: boolean = false;

  constructor(private storage: StorageService,
    private modalController: ModalController,
    private router: Router,
    private store: Store) { }

  ngOnInit() {
    this.friendsInfo$.subscribe(friendsInfo => {
      this.friendInfo = friendsInfo;
      console.log("data", this.friendInfo);
      if (this.friendInfo.length > 0) {
        this.searchResultFound = true;
      }
    });
  }

  change_callback(ele) {

  }

}
