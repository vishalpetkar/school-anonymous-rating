import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubmissionConfirmatoinComponent } from './submission-confirmatoin.component';

describe('SubmissionConfirmatoinComponent', () => {
  let component: SubmissionConfirmatoinComponent;
  let fixture: ComponentFixture<SubmissionConfirmatoinComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmissionConfirmatoinComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubmissionConfirmatoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
