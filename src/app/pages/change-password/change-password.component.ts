import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { SchoolServiceService } from 'src/app/services/school-service.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {

  public ChangeForm: FormGroup;

  constructor(private router: Router,
              public formBuilder: FormBuilder,
              public authService: SchoolServiceService,
              public loadingController: LoadingController) {

    this.ChangeForm = formBuilder.group({
      current_password: ['', [Validators.required]],
      new_password: ['', Validators.required],
      new_confirm_password: ['', Validators.required]
    });
  }

  ngOnInit() {}

  goToLogin() {
    this.router.navigate(['/login']);
  }

  change() {
    console.log(this.ChangeForm.value);
    
    this.authService.changePassword(this.ChangeForm.value).subscribe(success => {
        console.log(success);
        this.ChangeForm.reset();
        this.router.navigate(['/home']);
      }, err => {
    });
  }
}

