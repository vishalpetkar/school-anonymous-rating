import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-reward-popup',
  templateUrl: './reward-popup.component.html',
  styleUrls: ['./reward-popup.component.scss'],
})
export class RewardPopupComponent implements OnInit {

  price = 1000;
  constructor(
    public alertController: AlertController,
    private modalCtrl: ModalController) { }

  ngOnInit() {}

  async dismiss() {

    if (this.price < 1000) {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Alert',
        subHeader: '',
        message: 'Amount should be atleast 1000 cents.',
        buttons: [
          {
            text: 'Okay',
            role: 'ok',
            handler: () => {
              console.log('Okay');
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.modalCtrl.dismiss({
        'dismissed': true,
        price: this.price
      });
    }
  }
}
