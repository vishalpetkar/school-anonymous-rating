import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Profile } from 'src/app/models/school-model';
import { SendOTP, TempProfile } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {

  name: string = '';
  mobile: string = '';
  email: string = '';
  type: string = '';

  constructor(private router: Router,
    private store: Store) { }

  ngOnInit() {}

  async submit() {

    let profileData = new Profile();
    console.log(profileData);
    profileData.type = this.type;
    profileData.mobile = this.mobile;
    profileData.email = this.email;
    profileData.name = this.name;
    profileData.action = 'register';
    this.store.dispatch(new SendOTP({data: profileData})).subscribe(success => {
      console.log("profileData", profileData);
      this.store.dispatch(new TempProfile({data: profileData})).subscribe(success => {
        this.router.navigate(['/verification']);
      });
    });
  }

  login() {
    this.router.navigate(['/login']);
  }
}
