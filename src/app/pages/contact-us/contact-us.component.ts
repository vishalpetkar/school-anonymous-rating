import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { SendMessage } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
})
export class ContactUsComponent implements OnInit {

  message: string = '';
  constructor(private store: Store) { }

  ngOnInit() {}

  submit() {
    this.store.dispatch(new SendMessage({data: this.message})).subscribe(data => { });
  }
}
