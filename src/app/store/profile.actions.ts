import { Injectable } from '@angular/core';
import { State, Action, StateContext, NgxsSimpleChange, Select, Selector, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { patch, updateItem } from '@ngxs/store/operators';
import { Brand, LoginData, Notifications, Profile } from '../models/school-model';
import { SchoolServiceService } from '../services/school-service.service';
import { StorageService } from '../services/storage.service';
import { Constant } from '../constant';
import { FriendsInfo, RateChildren } from '../models/friend-model';

export class SendOTP {
static readonly type = 'Send OTP';
constructor(public payload: { data: Profile }) { }
}

export class VerifyOTP {
static readonly type = 'Verify OTP';
constructor(public payload: { data: Profile }) { }
}
  
export class AddProfile {
static readonly type = 'New Registration';
constructor(public payload: { data: Profile }) { }
}

export class UpdateProfile {
static readonly type = 'Update User Data On Server';
constructor(public payload: { data: Profile }) { }
}

export class SetProfile {
static readonly type = 'Set Profile';
constructor(public payload: { data: Profile }) { }
}

export class ViewProfile {
static readonly type = 'View Profile';
constructor(public payload: { data: Profile }) { }
}

export class SelectedModel {
static readonly type = 'Selected Model';
constructor(public payload: { data: string }) { }
}

export class GetProfile {
static readonly type = 'Login User';
constructor(public payload: { data: Profile }) { }
}

export class ClearProfile {
static readonly type = 'Logout User';
}

export class GetDashboard {
static readonly type = 'Get Dashboard';
}

export class Redirect {
  static readonly type = 'Redirect';
}

export class SendMessage {
  static readonly type = 'Send Message';
  constructor(public payload: { data: string }) { }
}

export class TempProfile {
  static readonly type = 'Temp Profile';
  constructor(public payload: { data: any }) { }
}

export class GetFriendsInfo {
  static readonly type = 'Get Friends Info';
  constructor(public payload: { data: FriendsInfo }) { }
}

export class AddFriendsInfo {
  static readonly type = 'Add Friends Info';
  constructor(public payload: { data: FriendsInfo }) { }
}

export class KeepFriendsInfo {
  static readonly type = 'Keep Friends Info';
  constructor(public payload: { data: FriendsInfo }) { }
}

export class AddRating {
  static readonly type = 'Add Rating';
  constructor(public payload: { data: RateChildren }) { }
}

export class GetBrand {
  static readonly type = 'Get Brand';
  constructor() { }
}

export class GetNotification {
  static readonly type = 'Get Notification';
  constructor() { }
}

export class SelectedBrand {
  static readonly type = 'Selected Brand';
  constructor(public payload: { data: Brand }) { }
}

export class PayGiftCard {
  static readonly type = 'Pay Gift Card';
  constructor(public payload: { data: {} }) { }
}

export interface ProfileModel {
    tempUser: Profile,
    selectedProfile: Profile,
    user: Profile;
    access_token: string;

    gift_card_resp: string;
    temp_friend_info: FriendsInfo;
    friends_info: Array<FriendsInfo>;
    brands: Array<Brand>;
    selected_brand: Brand;
    notifications: Array<Notifications>;
}

@State<ProfileModel>({
  name: 'profile',
  defaults: {
    tempUser: new Profile(),
    selectedProfile: new Profile(),
    user: new Profile(),
    access_token: '',

    gift_card_resp: '',
    temp_friend_info: new FriendsInfo(),
    friends_info: [],
    brands: [],
    selected_brand: new Brand(),
    notifications: []
  }
})
@Injectable()
export class ProfileState {

  userData: Profile;
  @Select(ProfileState) user$: Observable<LoginData>; 

  constructor(
    private store: Store,
    private router: Router,
    private storageService: StorageService,
    private authService: SchoolServiceService) {
    console.log('Profile state constructor..');
  }

  @Selector()
  static giftcardRespKey(state: ProfileModel) {
    return state.gift_card_resp;
  }

  @Selector()
  static brandKey(state: ProfileModel) {
    return state.brands;
  }

  @Selector()
  static notificationKey(state: ProfileModel) {
    return state.notifications;
  }

  @Selector()
  static tempFriendInfoKey(state: ProfileModel) {
    return state.temp_friend_info;
  }

  @Selector()
  static friendsInfoKey(state: ProfileModel) {
    return state.friends_info;
  }

  @Selector()
  static tempUserKey(state: ProfileModel) {
    return state.tempUser;
  }

  @Selector()
  static profileKey(state: ProfileModel) {
    return state.user;
  }

  @Selector()
  static accesstokenKey(state: ProfileModel) {
    return state.access_token;
  }

  @Selector()
  static selectedProfileKey(state: ProfileModel) {
    return state.selectedProfile;
  }

  @Selector()
  static isAuthenticated(state: ProfileModel): boolean {
    return !!state.access_token;
  }

  @Action(AddRating)
  addRating(ctx: StateContext<ProfileModel>, action: AddRating) {
    const state = ctx.getState();
    return this.authService.addRating(action.payload.data).pipe(
      tap(res => {
        ctx.patchState({
          ...state
        });
      })
    );
  }

  @Action(SendMessage)
  sendMessage(ctx: StateContext<ProfileModel>, action: SendMessage) {
    const state = ctx.getState();
    const obj = {user_id: state.user.id, message: action.payload.data};
    return this.authService.message(obj).pipe(
      tap(res => {
        ctx.patchState({
          ...state
        });
      })
    );
  }

  @Action(KeepFriendsInfo)
  keepFriendInfo(ctx: StateContext<ProfileModel>, action: KeepFriendsInfo) {
    const state = ctx.getState();
      ctx.patchState({
          ...state,
          temp_friend_info: action.payload.data
      });
  }

  @Action(GetFriendsInfo)
  getFriendInfo(ctx: StateContext<ProfileModel>, action: GetFriendsInfo) {
    const state = ctx.getState();
    return this.authService.searchFriendsInfo(action.payload.data).pipe(
      tap(res => {
        ctx.patchState({
            ...state,
            friends_info: res.data
        });
      })
    );
  }

  @Action(AddFriendsInfo)
  addFriendInfo(ctx: StateContext<ProfileModel>, action: AddFriendsInfo) {
    const state = ctx.getState();
    return this.authService.addFriendsInfo(action.payload.data).pipe(
      tap(res => {
        ctx.patchState({
            ...state,
            friends_info: res.data
        });
      })
    );
  }


  @Action(TempProfile)
  tempProfile(ctx: StateContext<ProfileModel>, action: TempProfile) {
    const state = ctx.getState();  
    ctx.patchState({
        ...state,
        tempUser: action.payload.data
    });
  }

  @Action(SendOTP)
  sendOTP(ctx: StateContext<ProfileModel>, action: SendOTP) {
    const state = ctx.getState();
    return this.authService.sendOTP(action.payload.data).pipe(
      tap(res => {
        console.log("response otp : ", res);

        let userData = new Profile();
        userData.otp = res.data;
        userData.email = action.payload.data.email;
        ctx.patchState({
            ...state,
            user: userData
        });
      })
    );
  }

  @Action(VerifyOTP)
  verifyOTP(ctx: StateContext<ProfileModel>, action: VerifyOTP) {
    const state = ctx.getState();
    return this.authService.verifyOTP(action.payload.data).pipe(
      tap(res => {
        console.log('after verify ', res);
        
        ctx.patchState({
            ...state,
            user: res.data
        });
      })
    );
  }

  @Action(AddProfile)
  addProfile(ctx: StateContext<ProfileModel>, action: AddProfile) {
    const state = ctx.getState();
    return this.authService.registration(action.payload.data).pipe(
      tap(res => {
        ctx.patchState({
            ...state,
            user: res.data
        });
      })
    );
  }

  @Action(SetProfile)
  setProfile(ctx: StateContext<ProfileModel>, action: SetProfile) {
    const state = ctx.getState();
    console.log('data before store in local storage', action.payload.data);
    this.storageService.setObject(action.payload.data, Constant.myLoginData);
    ctx.patchState({
      ...state,
      user: action.payload.data,
    });
  }

  @Action(UpdateProfile)
  updateProfile(ctx: StateContext<ProfileModel>, action: UpdateProfile) {
    const state = ctx.getState();
    return this.authService.updateProfile(action.payload.data).pipe(
      tap(res => {
        this.storageService.getObject(Constant.myLoginData).then(localLoginData => {
          localLoginData.user = res.data
          this.storageService.setObject(localLoginData, Constant.myLoginData)
          ctx.patchState({
            ...state,
            user: localLoginData.user,
            access_token : localLoginData.access_token
          });
          this.ngxsAfterBootstrap(ctx);
        });
      })
    );
  }

  @Action(GetProfile)
  getProfile(ctx: StateContext<ProfileModel>, action: GetProfile) {
    const state = ctx.getState();
    return this.authService.login(action.payload.data).pipe(
      tap(res => {
        console.log('after login', res);
        this.storageService.setObject(res.data, Constant.myLoginData).then(suc => {
          ctx.patchState({
            ...state,
            user: res.data.user,
            access_token : res.data.access_token
          });
          this.ngxsAfterBootstrap(ctx);
        });
      })
    );
  }

  @Action(ClearProfile)
  clearProfile(ctx: StateContext<ProfileModel>, action: ClearProfile) {
     const state = ctx.getState();
    return this.authService.logOut().pipe(
      tap(data => {
        ctx.setState({
            ...state,
            user: new Profile(),
            access_token: ''
        });
      })
    );
  }

  @Action(GetBrand)
  getBrand(ctx: StateContext<ProfileModel>, action: GetBrand) {
    const state = ctx.getState();
    return this.authService.getBrands().pipe(
      tap(res => {
        ctx.setState({
            ...state,
            brands: res.data
        });
      })
    );
  }

  @Action(GetNotification)
  getNotification(ctx: StateContext<ProfileModel>, action: GetNotification) {
    const state = ctx.getState();
    return this.authService.getNotifications().pipe(
      tap(res => {
        ctx.setState({
            ...state,
            notifications: res.data
        });
      })
    );
  }

  @Action(SelectedBrand)
  selectedBrand(ctx: StateContext<ProfileModel>, action: SelectedBrand) {
    const state = ctx.getState();
    ctx.patchState({
      ...state,
      selected_brand: action.payload.data
    });
  }

  @Action(PayGiftCard)
  payGiftCard(ctx: StateContext<ProfileModel>, action: PayGiftCard) {
    console.log('in gift card action = ' + action.payload.data);
    const state = ctx.getState();
    return this.authService.payGiftCard(action.payload.data).pipe(
      tap(res => {
        ctx.setState({
            ...state,
            gift_card_resp: res.data
        });
      })
    );
  }

  // LifeCycles
  ngxsOnInit(ctx: StateContext<ProfileModel>) {
    this.user$.subscribe(data => {
      console.log("init data", data);
    });
    const state = ctx.getState();
    this.storageService.getObject(Constant.myLoginData).then((data) => {
      if (data !== null) {
        return ctx.setState({
          ...state,
          user: data.user,
          access_token: data.access_token
        });
      }
      this.store.dispatch(new Redirect());
    });
  }

  ngxsOnChanges(change: NgxsSimpleChange) {
    // console.log('prev state', change.previousValue);
    // console.log('next state', change.currentValue);
  }

  ngxsAfterBootstrap(ctx: StateContext<ProfileModel>) {
    const state = ctx.getState();

    // this.authService.testemail().subscribe(data => {

    // })

    console.log('state', state);
    if (!!state.user.id) {
      if (state.user.type == 'parent') {
        // this.router.navigate(['/notifications']);
        this.router.navigate(['/search-children']);
      } else {
        // this.router.navigate(['/reward-option']);
        this.router.navigate(['/campus-aonoymous']);
      }
    } else {
      // this.router.navigate(['/reward-option']);
      this.router.navigate(['/login']);
    }
  }

}
