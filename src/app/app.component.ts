import { Component, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, Platform, IonRouterOutlet, ModalController, ActionSheetController, PopoverController, MenuController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Profile } from './models/school-model';
import { ClearProfile, ProfileState } from './store/profile.actions';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;

  @Select(ProfileState.profileKey) profile$: Observable<Profile>; 
  profileData: Profile = new Profile();

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  public appPages = [
    { title: 'Logout', url: '/landing', icon: 'home' }
  ];

  constructor(
    private store: Store,
    private router: Router,
    private menuController: MenuController,
    public modalController: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,
    public toastController: ToastController,
    private platform: Platform) {

    this.backButtonEvent();

  }

  ngOnInit() {
    this.profile$.subscribe(profile => {
      this.profileData = profile;
      console.log('=profile=', this.profileData);
    });
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(0, async() => {


      // close action sheet
      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          console.log('Action sheet');
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close popover
      try {
        const element = await this.popoverCtrl.getTop();
        if (element) {
          console.log('Popover controller');
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close modal
      try {
        const element = await this.modalController.getTop();
        if (element) {
          console.log('Modal');
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);
      }

      // close side menu
      try {
        const element = await this.menuController.getOpen();
        console.log(element);
        if (element !== undefined) {
          console.log('Side Menu');
          this.menuController.close();
          return;
        }
      } catch (error) {
      }

        try {
          this.routerOutlets.forEach(async(outlet: IonRouterOutlet) => {
            console.log('back => ', outlet, this.router.routerState.snapshot.url);
            
            if (!['/driver/home', '/admin/home', '/login'].some(x => x == this.router.routerState.snapshot.url)){

              // await this.location.back();
              console.log('done back');

            }else{
              if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
                this.lastTimeBackPress = new Date().getTime();
                this.toster("press again to exit from app");
              } else {
                navigator['app'].exitApp();
              }
            }
            return false;
        });
      } catch (error) {

      }
    });
  }

  async toster($msg){
    const toast = await this.toastController.create({
        message: $msg,
        duration: 1500
    });
    toast.present();
  }

  signOut() {
    this.store.dispatch(new ClearProfile()).subscribe(suc => {
      this.menuController.toggle();
      this.router.navigate(['/login']);
    });
  }
}
