
export class RateChildren {

    id: number;
    rating: number;
    message: string;

    constructor() {

        this.id = 0;
        this.rating = 0;
        this.message = '';
    }
}

export class RateChildrenData {

    status: string;
    message: string;
    data: string;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = '';
    }
}


export class FriendsInfo {

    id: number;
    city: string;
    state: string;
    country: string;
    name: string;
    email: string;
    phone: string;
    school: string;
    grade: string;
    message: string;
    reward: string;
    reward_price: number;
    lat: number;
    lng: number;

    constructor() {

        this.id = 0;
        this.city = '';
        this.state = '';
        this.country = '';
        this.name = '';
        this.email = '';
        this.phone = '';
        this.school = '';
        this.grade = '';
        this.message = '';
        this.reward = '';
        this.lat = 0;
        this.lng = 0;
    }
}

export class FriendsInfoData {

    status: string;
    message: string;
    data: Array<FriendsInfo>;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = [];
    }
}

