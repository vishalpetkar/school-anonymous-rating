export class Profile {
    id: number;
    name: string;
    email: string;
    mobile: string;
    password: string;
    confirm_password: string;
    address: string;
    state: string;
    district: string;
    taluka: string;
    pincode: string;
    device_id: string;
    accesstoken: string;
    otp: string;
    action: string;
    type: string;
    android_token: string;

    constructor() {
        this.id = 0;
        this.name = '';
        this.email = '';
        this.mobile = '';
        this.password = '';
        this.confirm_password = '';
        this.address = '';
        this.state = '';
        this.district = '';
        this.taluka = '';
        this.pincode = '';
        this.device_id = '';
        this.accesstoken = '';
        this.otp = '';
        this.action = '';
        this.type = '';
        this.android_token = '';
    }
}

export class ProfileData {

    status: string;
    message: string;
    data: Profile;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = new Profile();
    }
}

class Login {

    user: Profile;
    access_token: string;
    token_type: string;
    expires_in: string;

    constructor() {
        this.user = new Profile();
        this.access_token = '';
        this.token_type = '';
        this.expires_in = '';
    }
}

export class LoginData {

    status: string;
    message: string;
    data: Login;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = new Login();
    }
}

export class ChangePassword {
    
    mobile: string;
    current_password: string;
    new_password: string;
    new_confirm_password: string;
    
    constructor() {

        this.mobile = '';
        this.current_password = '';
        this.new_password = '';
        this.new_confirm_password = '';
    }
}

export class OtpData {

    status: string;
    message: string;
    data: string;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = '';
    }
}


export class MessageResult {

    status: string;
    message: string;
    data: string;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = '';
    }
}

export class Brand {
    brand_code: string;
    name: string;
    image_url: string;
    disclaimer: string;
}

export class BrandData {

    status: string;
    message: string;
    data: Array<Brand>;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = [];
    }
}

export class Notifications {

    id: number;
    child_name: string;
    from_user: number;
    to_user: number;
    message: string;
    reward: string;
    reward_price: string;
    rating: string;
    rating_message: string;
    gift_card_resp: string;
    sender_school: string;
    sender_grade: string;
    created: string;

    constructor() {
        this.id = 0;
        this.child_name = '';
        this.from_user = 0;
        this.to_user = 0;
        this.message = '';
        this.reward = '';
        this.reward_price = '';
        this.rating = '';
        this.rating_message = '';
        this.gift_card_resp = '';
        this.created = '';
    }
}

export class NotificationData {

    status: string;
    message: string;
    data: Array<Notifications>;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = [];
    }
}

export class GiftCardRespData {

    status: string;
    message: string;
    data: string;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = '';
    }
}
