import { Injectable } from '@angular/core';
import {  HttpRequest,  HttpHandler,  HttpEvent,  HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProfileState } from '../store/profile.actions';
import { Select } from '@ngxs/store';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {

  access_token = '';
  @Select(ProfileState.accesstokenKey) profile$: Observable<string>;
  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('request', request.url.includes('api-testbed.giftbit.com'));
    
    this.profile$.subscribe(token => {
      this.access_token = token;
    });

    if (request.url.includes('api-testbed.giftbit.com')) {
      request = request.clone({
        setHeaders: {
          'Authorization': `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJTSEEyNTYifQ==.elJiL1dlWUJFNysrQ3M2WUNyM0tNTTA0QUN6OGJoR3FHdXI3Vm9TRU5OblIwVEtpSmwrODRvMmw0dzh0VTEzcHh2b3NxY3lTYUpQOUQrMjg3Uk96OFowa1piOW5TWERTRlVqYW8zcjFBSDUxZmpjYmR6ck9RVGtZVVJUeHRweGs=.SYb18P9ALBmYhCWj23SLLGhydrxrSBNvyvsz+X5DfRM=`,
          'Content-Type': 'application/json; charset=utf-8',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
          'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
          'Access-Control-Allow-Credentials': 'true',
          'Access-Control-Max-Age': '86400'
        }
      });

    } else {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.access_token}`
        }
      });
    }
    return next.handle(request);
  }
}
