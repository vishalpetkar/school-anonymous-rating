import { 
    HttpEvent, 
    HttpInterceptor, 
    HttpHandler, 
    HttpRequest, 
    HttpErrorResponse, 
    HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { ClearProfile } from '../store/profile.actions';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    public loaderForGet = [
        'admin_dashboard',
        'vehicle_category'
    ];

    loader;
    constructor(
        private loadingController: LoadingController,
        private toastController: ToastController,
        private router: Router,
        private store: Store,
        private alertController: AlertController) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // Start Loader
        /*if (request.method !== "GET") {
            this.presentLoading();
        } else {
            console.log('request', request);

            if (this.loaderForGet.some(v => request.url.includes(v) )) {
                this.presentLoading();
            }
        }*/
        this.presentLoading();
        // ./ End Loader /.

        return next.handle(request)
        .pipe(

            // Handle Success Message
            tap(evt => {
                if (this.loader != undefined) {
                    this.loader.dismiss();
                }
                if (evt instanceof HttpResponse) {
                    if(evt.body && (evt.status==200 || evt.status==201) && evt.body.message != null) {
                        this.successToast(evt.body.message);
                    }
                }
            }),
            // ./ Handle Success Message /.

            // Handle Error Message
            catchError((error: HttpErrorResponse) => {
                if (this.loader != undefined) {
                    this.loader.dismiss();
                }
                let errorMsg = '';
                let errorCode = '';
                let code = 0;
                if (error.error instanceof ErrorEvent) {
                    errorMsg = `Error: ${error.error.message}`;
                }
                else {
                    errorMsg = `Message: ${error.error.message}`;
                    errorCode = `Code: ${error.status}`;
                    code = error.status;
                }
                this.errorModal(errorMsg, errorCode);
                
                if (code === 401) {
                    this.store.dispatch(new ClearProfile).subscribe(profile => {
                        this.router.navigate(['/login']);
                    });
                }

                return throwError(errorMsg);
            })
            // ./ Handle Error Message /.
        )
    }

    // Error Modal
    async errorModal(msg, code) {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Error',
            subHeader: '',
            message: msg,
            buttons: ['OK']
        });
        await alert.present();
    }

    // Success Toast
    async successToast(msg) {
        if (msg != "") {
            const toast = await this.toastController.create({
                message: msg,
                duration: 3000,
                position: 'middle'
            });
            toast.present();
        }
    }

    // Loader
    async presentLoading() {
        console.log('this.loader', this.loader);
        this.loader = await this.loadingController.create({
          cssClass: 'my-custom-class',
          message: 'Please wait...',
          duration: 3000
        });
        
        await this.loader.present();
    }
}
